#include"visalista.h"



meta *mkMeta(hyllplan *lager, hyllplan *pall,hyllplan *copyRestore){
 meta *m = malloc(sizeof(meta));
	if(!m) return NULL;
 m->lager = lager;
 m->pall = pall;
 m->copyRestore = copyRestore;

 return m;
}

link* make_node_linked(hyllplan* t) {
link *l = malloc(sizeof(link));
if (l == NULL) //Error handle
	return NULL;
l->next = NULL;
l->node = t;
return l;	
}

link* push(hyllplan* t, link* l) {
	link* tmp = make_node_linked(t);
	tmp->next = l;
	return tmp;
}
/*void fullShelf(vara *V,link *l){ 

	link *l = z;
	
	while(l !=NULL){

	if (strcmp(V->Lagerhylla,l->node->produkt->Namn) == 0){
		printf("\nDet finns redan en vara på denna plats");S
		return 0;
 		}
	
	 
		l = l->next;
	}
	return 0;
}

*/



link* skapa_lista(hyllplan *t, link *l){   // INORDERWALK BST
	if (t == NULL) return l;
	
	l= skapa_lista(t->right,l);
	l = push(t, l);
	l = skapa_lista(t->left,l);	
	return l;
}

link *append(link *l, hyllplan *t) {
 if(!l)
        return make_node_linked(t);
 if( !strcmp(t->Namn, l->node->Namn))      
        return l;
 l->next = append(l->next, t);
 return l;
}

link *dublett(link *l, hyllplan *t){
 if(!t) 
        return l;

 l = dublett(l, t->left);
 l = append(l, t);
 l = dublett(l, t->right);
 return l;
}




void visalista_samtliga(link *l){ 
	int i = 1;
	
	while(l !=NULL){
		 
		printf("\n\n%i. \t%s \t%s(\%i stycken)\n",i++,l->node->produkt->Namn,l->node->produkt->Lagerhylla,l->node->produkt->Antal);
		l = l->next;
 		
	}
	return;
}


int occurences(link *z, char Namn[1000]){ 

	int x = 0;
	link *l = z;
	
	while(l !=NULL){

	if (strcmp(Namn,l->node->produkt->Namn) == 0){
		 x++;
 		}
	 
		l = l->next;
	}
	return x;
}

link *redigera_samtliga_bes(link *z, char old_Name[1000],char new_bes[1000]){ 
	
	link *l = z;

	
	
	while(l !=NULL){

	if (!strcmp(old_Name,l->node->produkt->Namn)){
		 
		strcpy(l->node->produkt->Beskrivning, new_bes);
		
 		} 
	l = l->next;
	}
	return l;
}
link *redigera_samtliga_namn(link *z, char old_Name[1000],char new_name[1000]){ 
	
	link *l = z;

 
	while(l !=NULL){

	if (strcmp(old_Name,l->node->produkt->Namn) == 0){
		 
		strcpy(l->node->produkt->Namn, new_name);
		
 		} 
	l = l->next;
	
	}
	return l;
}
link *redigera_samtliga_pris(link *z, char old_Name[1000],int new_price){ 
	
	link *l = z;
 
	
	while(l !=NULL){

	if (!strcmp(old_Name,l->node->produkt->Namn)){
		 
		l->node->produkt->Pris = new_price;
		
 		} 
	l = l->next;
	}
	return l;
}









	
/*


*/
link *lista_hyllor(link *l, hyllplan *t, char Namn[1000]) {
if (t == NULL) return l;		
 l = lista_hyllor(l,t->right,Namn);
 if (strcmp(Namn,t->Namn) == 0) 
	l = push(t, l);
 l = lista_hyllor(l,t->left,Namn);	
 return l;
}

// har testat med att kopiera varan innan redigering, deleta den från trädet, sedan redigera varan som vi precis kopierade och bst-insert in i trädet igen med den uppdaterade varan
//Hittar varan v i träder t och erbjuder användaren att redigera fälten i varan.
//Om nyaVaran är NULL får anvädaren själv fylla i valfri information, annars används den som ges i ny vara.
hyllplan *redigera(hyllplan *t, vara *v, vara *nyaVaran) {
 if(t == NULL)
	return NULL;
 if(v == NULL)
	return NULL;

 if(strcmp(t->produkt->Namn, v->Namn) > 0)
	t->left = redigera(t->left, v,nyaVaran);
 else if(strcmp(t->produkt->Namn, v->Namn) < 0)
	t->right = redigera(t->right, v,nyaVaran);
 else {
	if(strcmp(t->produkt->Lagerhylla, v->Lagerhylla) > 0)
		t->left = redigera(t->left, v,nyaVaran);
	else if(strcmp(t->produkt->Lagerhylla, v->Lagerhylla) < 0)
		t->right = redigera(t->right, v,nyaVaran);
	else {
		char ans = 'j';
		while(ans == 'j' || ans == 'J') {
			if(nyaVaran == NULL) {
				printf("%s",v->Namn);
				printf("\n[N]amn");
				printf("\n[B]eskrivning");
				printf("\n[P]ris");
				printf("\n[L]agerhylla");
				printf("\nAn[t]al");
				printf(" \n \n Vad vill du ändra hos varan?:");
				char c;

				scanf("%s", &c);
				while(getchar() != '\n')
				;

				while (c == 'N' || c == 'n'){		
					printf("\nNuvarande namn: %s", v->Namn);   //första trädet körs över, varför?? 
					printf("\n-------------------------");
					printf("\nAnge nytt namn:");
					scanf("%s",v->Namn);
					while(getchar() != '\n')
					;
		
					visa_lista(t);
					return t;
				}
				while (c =='B' || c == 'b'){   //ändrar den verkligen på alla subtrees i o med att ingen ändring sker på namnet i do-while i bes_redigera o då vi söker i trädet med nyckeln som namn
	
					printf("\nNuvarande beskrivning: %s", v->Beskrivning);
					printf("\n-------------------------");
					printf("\nAnge ny beskrivning:");
					scanf("%s",v->Beskrivning); 
					while(getchar() != '\n')
					;

					visa_lista(t);
					return t;
				}
				while (c =='P' || c=='p'){
					printf("Nuvarande pris: %i", v->Pris);
					printf("\n-------------------------");
					printf("\nAnge nytt pris:");
					scanf("%i",&v->Pris);    // PROBLEM MED ATT SCANNA STORA INTS
					while(getchar() != '\n')
					;
		
					visa_lista(t);		
					return t;
				}
				while (c =='L' || c == 'l'){
		
					printf("Nuvarande lagerhylla: %s",v->Lagerhylla);
					printf("\n-------------------------");
					printf("\nAnge ny lagerplats:");
					scanf("%s",v->Lagerhylla);
					while(getchar() != '\n')
					;
	
					visa_lista(t);
					return t;
				}
				while (c == 't' || c =='T'){
			
					printf("Nuvarande antal: %i", v->Antal);
					printf("\n-------------------------");
					printf("\nAnge nytt antal:");
					scanf("%i",&v->Antal);
					while(getchar() != '\n')
					;
					visa_lista(t);
					return t;
			
				}
			}
			else {
				vara *tmp = t->produkt;
				t->produkt =  nyaVaran;
				clearProduct(tmp);
			}


			printf("Vill du ändra något mer?(J/N)\t");
			scanf("%c", &ans);
			while(getchar() != '\n') ;
		}
	}
 }
 return t;
}

int availableShelf (hyllplan *t, char Lagerhylla[10000]){
 link *l;
 l = skapa_lista(t,NULL);

 while(l != NULL){
 if(strcmp(l->node->produkt->Lagerhylla,Lagerhylla) == 0)return 1;
 l = l->next;
 }
 return 0;
}

vara *add_vara(hyllplan *t){
	
	//char Name[10000];
	char Lagerhyllan[10000];
	hyllplan *tmp;
	
	vara* V = malloc(sizeof(vara));
	if (!V)
		return NULL;


	printf("Namn på vara som ska lagras:\n");
	scanf("%s",V->Namn);
	/*if(char.IsLower(Name[0]) != 0){
		toupper(Name[0]);
		}
strcpy(V->Namn,Name);
	*/
	
	
	tmp = search(t,V->Namn);
	if(tmp == NULL) {	
		printf("Ange beskrivning av varan:\n");
		scanf("%s",V->Beskrivning);
		//term = &V->Beskrivning;
	
		printf("Ange pris för varan:\n");
		pris:
		if(scanf("%i", &V->Pris) != 1) {
		printf("Felinmatning %c, ange heltal\n",V->Pris);
		while(getchar() != '\n')
			;
		goto pris;
		}
	
		}
	else { 
		printf("\n%s finns redan i databsen",V->Namn);
		printf("\nAnvänder samma pris och beskrivning");
		V->Pris = tmp->produkt->Pris;
		strcpy(V->Beskrivning,tmp->produkt->Beskrivning);
		strcpy(V->Namn,tmp->produkt->Namn);
	}
	printf("\nAnge antal varor som skall lagras:\n");
	antal:
	if(scanf("%i", &V->Antal) != 1) {
		printf("Felinmatning %i, ange heltal\n",V->Antal);
		while(getchar() != '\n')
			;
		goto antal;
	}
		/*
		if(scanf("%i%c", &num, &term) != 2 || term != '\n'){
		printf("\nFelaktig inmatning %d",term);  // HÄR PRINTAS ALDRIG VAD DU KNAPPADE IN FÖR FEL
		printf("\nMata in nytt antal");
		while(getchar() != '\n')
		   ;
		goto antal;
		}
		else num = V->Antal;
		*/
	knappa:
	printf("Ange lagerhylla där varan skall lagras\n");
	scanf("%s",Lagerhyllan);
	while(getchar() != '\n')
	   ;	
	if (availableShelf(t,Lagerhyllan) > 0){
		printf("\nDet finns redan en produkt på denna hylla!");
		goto knappa;
		}
	
		/*
		while(getchar() != '\n')
		   ;
	
		char test = terme[0];
		const char *from = terme;
		char *to = (char*) malloc(sizeof(terme)-1);
		strncpy(to, from+1,sizeof(terme));
		int *nrr = atoi(terme);
	
		if(isalpha(test) && isupper(test) && check_int(nrr)){
		memcpy(V->Lagerhylla,terme,sizeof(terme));
		}
		else {	
		printf("\nFelaktig inmatning %c",term);
		printf("\nMata in ny plats i lagret ");
		while(getchar() != '\n')
	   	;
		goto lager;
		}
		*/
	strcpy(V->Lagerhylla,Lagerhyllan);	
	
return V;	
}


vara *pickProductFromList(link *listOfChoices, int mode){
int save = 0;
char ans;
int n;
int ansNR;
link *reiterate = listOfChoices;

if (listOfChoices == NULL){
 printf("Databasen är tom!");
 return NULL;
}

while(1) {
	if(mode == NAMES) {
		for(n = 1; n <= 20 && listOfChoices!= NULL; n++) {
			printf("\n%i, %s\n", n, listOfChoices->node->produkt->Namn);
			listOfChoices = listOfChoices->next; 
		}
	printf("Välj en vara [1-20],gå till [n]ästa visning eller [a]vbryt ");
	}
	else if(mode == SHELVES) {
		for(n = 1; n <= 20 && listOfChoices!= NULL; n++) {
			printf("%i, [%s, %s]\n", n, listOfChoices->node->produkt->Namn, listOfChoices->node->produkt->Lagerhylla);
			listOfChoices = listOfChoices->next; 
		}
	printf("Välj rätt hylla för varan [1-20],gå till [n]ästa visning eller [a]vbryt ");

	}
	
	scanf("%s", &ans);

	while(getchar() != '\n') ;
	ansNR = atoi(&ans);
	
	if(ans == 'n' || ans == 'N') save += 20;

	else if(ans == 'a' || ans == 'A') return NULL;
		
	else if(1 <= ansNR &&  ansNR <= 20) {
		for(n = 1; n < (save + ansNR) && reiterate != NULL; n++)
			reiterate = reiterate->next;
			
		if(!reiterate) return NULL;
		return reiterate->node->produkt;
	}
	
}

 return reiterate->node->produkt;
}

//Binds multiple choices together to pick an item at a specific shelf
 vara *pickProduct(hyllplan* t) {
 link *listOfNames = dublett(NULL,t); //No double entries for the same name
 vara *chosen = pickProductFromList(listOfNames, NAMES);

 if(!chosen) return NULL;

 link *listOfShelves =  lista_hyllor(NULL,t,chosen->Namn);
 chosen =  pickProductFromList(listOfShelves, SHELVES);


 return chosen;
}






void lista_allt_pall(hyllplan *t){

 fflush(stdin);
 int i;

 link *lena;
 lena = dublett(NULL,t);


 if (t == NULL){
	printf("\n\n Din pall är tom\n");
	
 }	
 
 else {
	printf("\n\nVaror i din pall");

	i = 1;
 	while(lena != NULL) {
	printf("\n%i.\t%s\t%i",i++,lena->node->produkt->Namn,lena->node->produkt->Antal);
	lena = lena->next;

 	} 
  }
 return;
  }

//Använd l = NULL;




link *pop_node(link *z) {
 
link *tmp;

tmp = z->next;

free(z);

return tmp;


}

//meta *updateRestore(m){




int price_each_item(hyllplan *t,char Namn[1000]){   // INORDERWALK IN BST
	
	int i = 0;	
	if (t == NULL) return 0;
	
	i += price_each_item(t->left,Namn);
	
	if(strcmp(Namn,t->produkt->Namn) == 0){
	
	i+=(t->produkt->Antal)*(t->produkt->Pris);
	}
	i+=price_each_item(t->right,Namn);
	
	return i;
}


int summeringen(hyllplan *t,char Namn[1000]){   // INORDERWALK IN BST

	int i = 0;	
	if (t == NULL) return 0;
	
	i += summeringen(t->left,Namn);
	
	if(strcmp(Namn,t->produkt->Namn) == 0){
	i+=t->produkt->Antal;
	}
	i+=summeringen(t->right,Namn);
	return i;
}

int total_pris(hyllplan *t, char Namn[1000]){

	int i = 0;	
	if (t == NULL) return 0;
	
	i += summeringen(t->left,Namn);
	
	if(strcmp(Namn,t->produkt->Namn) == 0){
	i+=(t->produkt->Pris)*t->produkt->Antal;
	}
	i+=summeringen(t->right,Namn);
	return i;
}

vara *producera(int antal, vara *produkt){

vara *geida;
geida = malloc(sizeof(vara));
memcpy(geida,produkt,sizeof(vara));
geida->Antal = antal;

return geida;
}
//Changes the number in storage for the given vara to the int 'antal'.
hyllplan *change_nr(hyllplan *t, vara *packa, int rest){
 
 if(t == NULL)
	return NULL;

 if (strcmp(t->produkt->Namn, packa->Namn) == 0) {
 	if(strcmp(t->produkt->Lagerhylla, packa->Lagerhylla) == 0)
		t->produkt->Antal = rest;
	
	else if(strcmp(t->produkt->Lagerhylla, packa->Lagerhylla) > 0)
		t->left = change_nr(t->left, packa, rest);
	else 
		t->right = change_nr(t->right, packa, rest);
 }

 else if(strcmp(t->produkt->Namn, packa->Namn) > 0)
	t->left = change_nr(t->left, packa, rest);
 else
	t->right = change_nr(t->right, packa, rest);

 return t;	
}







void clear_list(link *l) {

 link *tmp;

 while(l != 0) {
 tmp = l->next;
 l->node = NULL;
 l->next = NULL;
 free(l);
 l = tmp;
 }

}



void utCheck(hyllplan *t){

link *lena = skapa_lista(t,NULL);
int pris = 0;
lista_allt_pall(t);

while (lena!=NULL) {
pris += price_each_item(t,lena->node->produkt->Namn);
lena = lena->next;

}
printf("\nTotalt pris för pallen %i",pris);

printf("\nLagerplatser för att packa pallen");


visalista_samtliga(skapa_lista(t,NULL));
t = NULL;
free(t);
return;
}




meta *extract(meta *m, vara *packas,int antal){

int rest;
link *lena = skapa_lista(m->lager,NULL);

while(lena != NULL && antal > 0) {
//Insertar samma vara flera ggr om varan har fler lagerplatser
 if (strcmp(lena->node->produkt->Namn,packas->Namn) == 0){
	if(lena->node->produkt->Antal > antal){
		rest = (lena->node->produkt->Antal)- antal;
		m->lager = change_nr(m->lager,lena->node->produkt,rest);
		packas->Antal = antal;
		strcpy(packas->Lagerhylla,lena->node->produkt->Lagerhylla); 
		m->pall = insert(m->pall,packas);
		antal = 0;
		//return m;
	}
	
	if(lena->node->produkt->Antal == antal){
		packas->Antal = antal;
		strcpy(packas->Lagerhylla,lena->node->produkt->Lagerhylla);
		m->pall = insert(m->pall,packas);
		m->lager = delete(m->lager,lena->node->produkt);
		antal = 0;
		//return m;
	}

	if(lena->node->produkt->Antal < antal){
		antal -= lena->node->produkt->Antal;
		packas->Antal = lena->node->produkt->Antal;
		strcpy(packas->Lagerhylla,lena->node->produkt->Lagerhylla); 
		m->pall = insert(m->pall,packas);
		m->lager = delete(m->lager,lena->node->produkt);	
	}
 	}//
	lena = lena->next;
 }
 return m;	
}


meta *packa_pall(meta *m){
fflush(stdin);
int i;
int nr;
char ans;

link *leif;
leif = dublett(NULL,m->pall);

 if (m->pall == NULL) 
	printf("\nDin pall är tom\n\n");
 
else { 
	printf("\n\nDin pall innehåller");
	for (i = 1;leif != NULL; i++) {
		printf("\n\n-\t%s (%i)",leif->node->produkt->Namn, summeringen(m->pall, leif->node->produkt->Namn));
		leif = leif->next;
		

	 }
 //clear_list(leif);

 } 
 printf("\n\n\n");
 vara *vald = pickProductFromList(dublett(NULL,m->lager),NAMES);
 if(vald == NULL){
 printf("Ingen vara vald! Avslutar");
 return m;	
}
 int totalt = summeringen(m->lager,vald->Namn);
 printf("\nNamn: %s",vald->Namn);  
 printf("\nAntal: %i",totalt); // SUMMAN AV ALLLA ANTAL SOM FINNS PÅ DE MÖJLIGA OLIKA PLATSERNA

 again:
 printf("\n\nVälj antal (0--%i)",totalt);
 scanf("%s",&ans);      
 while(getchar() != '\n')
	;		             
 nr = strtol(&ans, NULL, 10);
 
 printf("\nI read %i\n", nr);
 if (nr <= 0)  return m;
 if (nr > totalt){
	printf("För stort önskat antal (%i) att packa på pallen", nr);
	goto again;
 }
 vara *pack = copy_produkt(vald);
 m = extract(m, pack,nr);
 
 return m;
 
 

 


}





int list_counter(link *currentlystorage, link *storagebefore) {
 int currently = 0;
 int before = 0;
 while (currentlystorage !=NULL) {   
	currently ++;
	currentlystorage = currentlystorage->next;
 }
 while(storagebefore !=NULL){ 
	before ++;
	storagebefore = storagebefore->next;  
 }
 if(currently > before)  //adderats vara i trädet
	return 2;
 else if(currently < before) //tagits bort vara i trädet
	return 1;
 else if(currently == before) // redigerats vara i trädet
	return 0;
 else //catch error
	return -1;
}//method
 

hyllplan *copyTotalTree(meta *m){

 vara *v;

 link *currentlystorage = skapa_lista(m->lager,NULL);

 while(currentlystorage !=NULL) {

 v = copy_produkt(currentlystorage->node->produkt);
 m->copyRestore = insert(m->copyRestore,v);
 currentlystorage = currentlystorage->next;
 }
 return m->copyRestore;
} 



hyllplan *undo(meta *m){

 int choice;
 
 link *currentlystorage = skapa_lista(m->lager,NULL);
 link *storagebefore = skapa_lista(m->copyRestore,NULL);

 choice = list_counter(currentlystorage,storagebefore);

 if(choice == 2) {                // då har det ADDERATS en vara! vi måste alltså ta reda på den adderade varan och bst- deleta det a, 
	while (currentlystorage !=NULL || storagebefore !=NULL) {
		if(strcmp(currentlystorage->node->produkt->Namn,storagebefore->node->produkt->Namn) == 0) {
			currentlystorage = currentlystorage->next;
			storagebefore = storagebefore->next;
		}
		else {
			m->lager = delete(m->lager,currentlystorage->node->produkt);
			return m->lager;
		}
        }//while
 }
 else if (choice == 1) {   // då har en vara RADERATS!, vi måste alltså inserta den borttagna varan 
	while (currentlystorage !=NULL || storagebefore !=NULL) {
		if(strcmp(currentlystorage->node->produkt->Namn,storagebefore->node->produkt->Namn) == 0){
			currentlystorage = currentlystorage->next;
			storagebefore = storagebefore->next;
		}
		else {
			printf("\nHEJEHEJEHEJEEE");
			m->lager = insert(m->lager,currentlystorage->node->produkt);
			return m->lager;
		}
	}//while
 }//if
 else if(choice == 0) { // då har det redigerats! vi måste först börja jämföra namn sen beskrivning osv.
	while (currentlystorage !=NULL || storagebefore !=NULL) {

		if(strcmp(currentlystorage->node->produkt->Namn,storagebefore->node->produkt->Namn) != 0)
			 strcpy(currentlystorage->node->produkt->Namn,storagebefore->node->produkt->Namn);

		if(strcmp(currentlystorage->node->produkt->Beskrivning,storagebefore->node->produkt->Beskrivning) != 0) 
			strcpy(currentlystorage->node->produkt->Beskrivning,storagebefore->node->produkt->Beskrivning);
		
		if(strcmp(currentlystorage->node->produkt->Lagerhylla,storagebefore->node->produkt->Lagerhylla) != 0) 
			strcpy(currentlystorage->node->produkt->Lagerhylla,storagebefore->node->produkt->Lagerhylla);

		if(currentlystorage->node->produkt->Antal != storagebefore->node->produkt->Antal) 
			currentlystorage->node->produkt->Antal = storagebefore->node->produkt->Antal;
		
		if(currentlystorage->node->produkt->Pris != storagebefore->node->produkt->Pris) 
			currentlystorage->node->produkt->Pris = storagebefore->node->produkt->Pris;

		currentlystorage = currentlystorage->next;
		storagebefore = storagebefore->next;
		
		return m->lager;
	}//while
 }//if	
return m->lager;
}//method

/*
vara *summering(){

hyllplan *tmp;
tmp = search(t, Namn);

if (tmp->produkt->Antal > val)

t->produkt->Antal -= val;



*/



/*
int main(){

 
 meta *m = mkMeta(NULL, NULL);
 m->lager = malloc(sizeof(hyllplan));
 	if(!m->lager) return -1;
 m->lager = createTREE();
 m->pall = malloc(sizeof(hyllplan));
	if(!m->pall) return -1;
 m->pall = NULL;
 m->lager = insert(m->lager,add_vara(m->lager));
 link *l;
 l = skapa_lista(m->lager,NULL);
 redigera_vara(m->lager,l);
 //tabort(l,m->lager);
 

return 0;
}
*/


// funak så att knappen länkar till rätt valda vara
