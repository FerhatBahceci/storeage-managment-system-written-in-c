#ifndef VARA_H_
#define VARA_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>



typedef struct Vara {

char Namn[10000];
char Beskrivning[10000];	
int Pris;
char Lagerhylla[10000];
int Antal;
	
} vara;


bool check_int(int *nr);

void remove_vara(vara* V);

void visa(vara* V);

void clearProduct(vara *p);

#endif
