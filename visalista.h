#ifndef VISALISTA_H_
#define VISALISTA_H_
#define NAMES 0
#define SHELVES 1



#include"bst_hylla.h"


typedef struct linked_list {
hyllplan *node;
struct linked_list *next;

}link;

typedef struct meta {
hyllplan *lager;
hyllplan *pall;
hyllplan *copyRestore;
} meta;



hyllplan *undo(meta *m);

int list_counter(link *currently, link *before);

meta *mkMeta(hyllplan *lager, hyllplan *pall,hyllplan *copyRestore);

link* make_node_linked(hyllplan* t);

link* push(hyllplan* t, link* l);

link* skapa_lista(hyllplan *t, link *l);

link *append(link *l, hyllplan *t);

link *dublett(link *l, hyllplan *t);

void visalista_samtliga(link *l);

int occurences(link *z, char Namn[1000]);

link *redigera_samtliga_bes(link *z, char old_Name[1000],char new_bes[1000]);

link *redigera_samtliga_namn(link *z, char old_Name[1000],char new_name[1000]);

link *redigera_samtliga_pris(link *z, char old_Name[1000],int new_price);

link *lista_hyllor(link *l, hyllplan *t, char Namn[1000]);

hyllplan *redigera(hyllplan *t, vara *v, vara *nyaVaran);

hyllplan *visning (link *l, hyllplan *t);

vara *pickProductFromList(link *listOfChoices, int mode);

vara *pickProduct(hyllplan* t);

link *lista_allt(hyllplan *t, link *l);

hyllplan *redigera_vara(hyllplan *t,link *l);

link *pop_node(link *z);

hyllplan *tabort (link *l, hyllplan *t);

int summeringen(hyllplan *t,char Namn[1000]);

int total_pris(hyllplan *t, char Namn[1000]);

vara *producera(int antal, vara *produkt);

hyllplan *change_nr(hyllplan *t, vara *packa, int rest);

meta *packning(meta *m, int antal, vara *produkt);

void clear_list(link *l);

meta *packa_pall(meta *m);

meta *extract(meta *m, vara *packas,int antal);

hyllplan *createTREE();

void utCheck(hyllplan *t);

void lista_allt_pall(hyllplan *t);

int availableShelf (hyllplan *t, char Lagerhylla[10000]);

vara *add_vara(hyllplan *t);



#endif












