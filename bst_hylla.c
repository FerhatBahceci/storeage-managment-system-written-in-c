#include"bst_hylla.h"


hyllplan* find_min(hyllplan *t) {
	if (t->left)
		return find_min(t->left);
	else
	 return t;
}

void visa_lista(hyllplan *t){   // INORDERWALK IN BST
	if (t == NULL) return;
	
	visa_lista(t->left);
	
	printf("%s:%s:%i:%s:%i\n",t->produkt->Lagerhylla,t->produkt->Namn,t->produkt->Antal, t->produkt->Beskrivning, t->produkt->Pris);
	visa_lista(t->right);
	return;
}

hyllplan *make_node(vara* produkt) {
hyllplan *t;
t = malloc(sizeof(hyllplan));
if (t == NULL) //Error handle
	return NULL;
t->right = NULL;
t->left = NULL;
strcpy(t->Namn,produkt->Namn); 
t->produkt = produkt;
return t;
}


hyllplan* insert(hyllplan *t,vara *produkt){
if (t == NULL)
	return make_node(produkt);

else if (strcmp(t->Namn,produkt->Namn) == 0){
	if (strcmp(t->produkt->Lagerhylla,produkt->Lagerhylla) == 0){
		t->produkt->Antal += produkt->Antal;
		return  t;
	}
	if (strcmp(t->produkt->Lagerhylla,produkt->Lagerhylla) < 0) t->right = insert(t->right,produkt);

	if (strcmp(t->produkt->Lagerhylla,produkt->Lagerhylla) > 0) t->left = insert(t->left, produkt);

}
else if (strcmp(t->Namn, produkt->Namn) > 0)
	t->left = insert(t->left, produkt);

else if (strcmp(t->Namn, produkt->Namn) < 0)
	t->right = insert(t->right,produkt);

return t;
}




vara *copy_produkt(vara *v){

vara *x = malloc(sizeof(vara));

strcpy(x->Namn,v->Namn);
strcpy(x->Beskrivning,v->Beskrivning);
strcpy(x->Lagerhylla,v->Lagerhylla);
x->Pris = v->Pris;
x->Antal = v->Antal;

return x;
}


hyllplan *copy_hyllplan(hyllplan *t){

hyllplan *x = malloc(sizeof(hyllplan));

strcpy(x->Namn,t->Namn);
x->left = t->left;
x->right = t->right;
x->produkt = copy_produkt(t->produkt);
return x;
}


hyllplan* delete(hyllplan* t,vara *produkt){

 if (t== NULL) return t; 

 else if (strcmp(t->Namn,produkt->Namn) < 0 )   //denna del går till rätt ställe
	t->right = delete(t->right, produkt);
 else if (strcmp(t->Namn, produkt->Namn) > 0)
	t->left = delete(t->left, produkt);
 else {
	if(strcmp(t->produkt->Lagerhylla,produkt->Lagerhylla) > 0) 
		t->left = delete(t->left, produkt);
	else if(strcmp(t->produkt->Lagerhylla,produkt->Lagerhylla) < 0) 
		t->right = delete(t->right, produkt);
	else {
		if (t->left != NULL && t->right !=NULL) { //om noden vi ska ta bort har 2st barn, ersätt den nod som ska tas bort med den misnta till höger subtree 
			hyllplan *tmp = find_min(t->right);
			hyllplan *tmp_cpy = copy_hyllplan(tmp);
			tmp_cpy->right = delete(t->right, tmp->produkt);
			tmp_cpy->left = t-> left;
			free(t->produkt);
			free(t);	
			return tmp_cpy;
		
			}
		else if (t->left == NULL && t->right !=NULL) { //om noden vi ska ta bort har ett barn till höger men inget till vänster,
			hyllplan *tmp = t->right;
			free(t->produkt);
			free(t);
			return tmp;
			}
		else if (t->left != NULL && t->right == NULL) {// om noden vi ska ta bort har ett barn till vänster men inget till höger
			hyllplan *tmp = t->left;
			free(t->produkt);
			free(t);
			return tmp;
		
			}	
		else { 
			free(t->produkt);
			free(t);
			return NULL;
		
			}
	}
 }
return t;
}





hyllplan *search(hyllplan *t, char Namn[1000]){

if (t == NULL) return NULL;

if (strcmp(t->produkt->Namn,Namn) == 0) return t;

if (strcmp(t->produkt->Namn,Namn) > 0) return search(t->left,Namn);

if (strcmp(t->produkt->Namn,Namn) < 0) return search(t->right,Namn);

return t;

}




/*

bool check_int(int *nr){
int i = 0;

for(i;sizeof(nr)-1;i++){

	if(isdigit(nr[i]))
	return true;
}

return false;
	}
	*/


