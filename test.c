#include "test.h"
hyllplan *createTREE(){

 hyllplan* t = malloc(sizeof(hyllplan));
	if(!t) return NULL;


 vara *v1 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v1->Namn,"Kaffe"); 
 sprintf(v1->Beskrivning,"Mellanrost");
 sprintf(v1->Lagerhylla,"A12");
 v1->Antal = 10;
 v1->Pris = 23;

 vara *v2 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v2->Namn,"Hamburgarbröd"); 
 sprintf(v2->Beskrivning,"Pågen");
 sprintf(v2->Lagerhylla,"C23");
 v2->Antal = 36;
 v2->Pris = 25;


 vara *v3 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v3->Namn,"Champinjoner"); 
 sprintf(v3->Beskrivning,"Polen");
 sprintf(v3->Lagerhylla,"X50");
 v3->Antal = 12;
 v3->Pris = 30;

 vara *v4 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v4->Namn,"Bananer"); 
 sprintf(v4->Beskrivning,"Kuba");
 sprintf(v4->Lagerhylla,"X40");
 v4->Antal = 100;
 v4->Pris = 5;

 vara *v5 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v5->Namn,"Dadlar"); 
 sprintf(v5->Beskrivning,"Saudiarabien");
 sprintf(v5->Lagerhylla,"X1");
 v5->Antal = 100;
 v5->Pris = 30;

 vara *v6 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v6->Namn,"Apelsin"); 
 sprintf(v6->Beskrivning,"Spanien, Barcelona");
 sprintf(v6->Lagerhylla,"X441");
 v6->Antal = 133;
 v6->Pris = 20;


 vara *v7 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v7->Namn,"Rostbiff,Sverige,Uppsala"); 
 sprintf(v7->Beskrivning,"Kuba");
 sprintf(v7->Lagerhylla,"B1100000000");
 v7->Antal = 100;
 v7->Pris = 144;


 vara *v8 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v8->Namn,"Kyckling"); 
 sprintf(v8->Beskrivning,"Sverige,Malmö");
 sprintf(v8->Lagerhylla,"B31");
 v8->Antal = 100;
 v8->Pris = 144;

vara *v9 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v9->Namn,"Lax"); 
 sprintf(v9->Beskrivning,"Norge,Trondheim");
 sprintf(v9->Lagerhylla,"B666666666666");
 v9->Antal = 333333;
 v9->Pris = 99;

vara *v10 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v10->Namn,"Lövbiff"); 
 sprintf(v10->Beskrivning,"Sverige, Jokkmokk");
 sprintf(v10->Lagerhylla,"B26356450");
 v10->Antal = 9524;
 v10->Pris = 1;

vara *v11 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v11->Namn,"Kalkonbröst filé"); 
 sprintf(v11->Beskrivning,"Sverige,Malmö");
 sprintf(v11->Lagerhylla,"B81341");
 v11->Antal = 3;
 v11->Pris = 23131;


vara *v12 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v12->Namn,"Peak jacka"); 
 sprintf(v12->Beskrivning,"Sverige,Stockholm");
 sprintf(v12->Lagerhylla,"K35141");
 v12->Antal = 5151;
 v12->Pris = 4000;

vara *v13 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v13->Namn,"Tennis jacka"); 
 sprintf(v13->Beskrivning,"Sverige,Stockholm");
 sprintf(v13->Lagerhylla,"K750");
 v13->Antal = 88;
 v13->Pris = 4000123;

vara *v14 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v14->Namn,"Ralph Lauren badbyxor"); 
 sprintf(v14->Beskrivning,"USA,Chicago");
 sprintf(v14->Lagerhylla,"K112");
 v14->Antal = 3;
 v14->Pris = 78215124;

vara *v15 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v15->Namn,"Parajumpers jacka"); 
 sprintf(v15->Beskrivning,"USA,N.Y");
 sprintf(v15->Lagerhylla,"K30322");
 v15->Antal = 5151;
 v15->Pris = 4000;

vara *v16 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v16->Namn,"Canada Goose jacka"); 
 sprintf(v16->Beskrivning,"Sverige,Stockholm");
 sprintf(v16->Lagerhylla,"K80");
 v16->Antal = 5151;
 v16->Pris = 878434000;

vara *v17 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v17->Namn,"Tiger jacka"); 
 sprintf(v17->Beskrivning,"Sverige,Stockholm");
 sprintf(v17->Lagerhylla,"K30");
 v17->Antal = 5151;
 v17->Pris = 4000;

vara *v18 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v18->Namn,"Yxa"); 
 sprintf(v18->Beskrivning,"Miniyxa med gummihandtag");
 sprintf(v18->Lagerhylla,"V30");
 v18->Antal = 74151;
 v18->Pris = 3413513413;

vara *v19 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v19->Namn,"Hammare"); 
 sprintf(v19->Beskrivning,"30cm total längd");
 sprintf(v19->Lagerhylla,"V11");
 v19->Antal = 76151;
 v19->Pris = 34113;

vara *v20 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v20->Namn,"Motorsåg"); 
 sprintf(v20->Beskrivning,"Drivs på olja");
 sprintf(v20->Lagerhylla,"V33321");
 v20->Antal = 761541;
 v20->Pris = 3413513413;


vara *v21 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v21->Namn,"Skruvmejsel"); 
 sprintf(v21->Beskrivning,"SDASDADADADADAAAAAAAA");
 sprintf(v21->Lagerhylla,"X53213");
 v21->Antal = 82;
 v21->Pris = 10000;

vara *v22 = malloc(sizeof(vara));
	if(!v1) return NULL;
 sprintf(v22->Namn,"Skruvmejsel"); 
 sprintf(v22->Beskrivning,"SDASDADADADADAAAAAAAA");
 sprintf(v22->Lagerhylla,"V53");
 v22->Antal = 18;
 v22->Pris = 10000;

 t = insert(NULL,v1);
 t = insert(t,v2);
 t = insert(t,v3);
 t = insert(t,v4);
 t = insert(t,v5);
 t = insert(t,v6);
 t = insert(t,v7);
 t = insert(t,v8);
 t = insert(t,v9);
 t = insert(t,v10);
 t = insert(t,v11);
 t = insert(t,v12);
 t = insert(t,v13);
 t = insert(t,v14);
 t = insert(t,v15);
 t = insert(t,v16);
 t = insert(t,v17);
 t = insert(t,v18);
 t = insert(t,v18);
 t = insert(t,v19);
 t = insert(t,v20);
 t = insert(t,v21);
 t = insert(t,v22);


 return t;

}
