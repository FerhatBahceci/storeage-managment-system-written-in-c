#ifndef BST_HYLLA_H_
#define BST_HYLLA_H_

#include"vara.h"


typedef struct hylla {
vara* produkt;
char Namn[100];
struct hylla * right, * left;

} hyllplan;


hyllplan* find_min(hyllplan *t);

void visa_lista(hyllplan *t);

hyllplan *make_node(vara* produkt);

hyllplan* insert(hyllplan *t,vara* produkt);

vara *copy_produkt(vara *v);

hyllplan *copy_hyllplan(hyllplan *t);

hyllplan* delete(hyllplan* t,vara *produkt);

hyllplan *search(hyllplan *t, char Namn[1000]);


#endif





